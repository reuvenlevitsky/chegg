package homework.chegg.com.chegghomework.utils.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
    public interface OnItemClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);

        void onHighlight(View view, int position);

        void onUnhiglight(View view, int position);
    }

    private OnItemClickListener mListener;

    private GestureDetector mGestureDetector;

    public RecyclerItemClickListener(Context context, final RecyclerView recyclerView, final OnItemClickListener listener) {
        mListener = listener;

        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public void onShowPress(MotionEvent e) {
                View childView = getView(recyclerView, e);

                if (childView != null && mListener != null) {
                    listener.onHighlight(childView, getIndexForChild(recyclerView, childView));
                }
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                View childView = getView(recyclerView, e);

                if (childView != null && mListener != null) {
                    listener.onUnhiglight(childView, getIndexForChild(recyclerView, childView));
                }

                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                View childView = getView(recyclerView, e);

                if (childView != null && mListener != null) {
                    mListener.onItemLongClick(childView, getIndexForChild(recyclerView, childView));
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent e) {
        View childView = getView(recyclerView, e);

        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, getIndexForChild(recyclerView, childView));
        }

        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    }

    private View getView(RecyclerView recyclerView, MotionEvent e) {
        View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
        return childView;
    }

    private int getIndexForChild(RecyclerView recyclerView, View childView) {
        return recyclerView.getChildAdapterPosition(childView);
    }
}
