package homework.chegg.com.chegghomework.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import homework.chegg.com.chegghomework.R;
import homework.chegg.com.chegghomework.model.Article;
import homework.chegg.com.chegghomework.utils.recyclerview.Bindable;

/**
 * Created by reuvenlevitsky on 24/12/2016.
 */

/**
 * This is the Custom View class we use for the card cell.
 */
@EViewGroup(R.layout.card_item)
public class CardItemCellView extends RelativeLayout implements Bindable<Article> {

    // ------------------------------ Vars ------------------------------ //

    @ViewById(R.id.imageView_card_item)
    ImageView imageView;

    @ViewById(R.id.textView_card_item_title)
    TextView title;

    @ViewById(R.id.textView_card_item_subtitle)
    TextView subtitle;

    // ------------------------------ Constructors ------------------------------ //

    public CardItemCellView(Context context) {
        super(context);
    }

    public CardItemCellView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CardItemCellView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    // ------------------------------ Bindable ------------------------------ //

    @Override
    public void bind(Article bindingObject) {
        this.title.setText(bindingObject.getTitle());
        this.subtitle.setText(bindingObject.getSubtitle());

        Picasso.with(getContext()).load(bindingObject.getImageUrl()).into(this.imageView);
    }

    // ------------------------------ Private ------------------------------ //

    @AfterViews
    void initView(){
        // Add here setup

    }

}
