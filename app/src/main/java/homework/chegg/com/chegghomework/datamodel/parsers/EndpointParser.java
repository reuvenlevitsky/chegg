package homework.chegg.com.chegghomework.datamodel.parsers;

import android.support.annotation.Nullable;

import java.util.List;

import homework.chegg.com.chegghomework.model.Article;
import io.reactivex.Observable;

/**
 * Created by reuvenlevitsky on 05/03/2018.
 */

public interface EndpointParser {

    /**
     *
     * @param object This json object is being parsed from the network request. Possible values are (Arraylist, Map, rest of the primitives).
     * @return Observable for loading a list of articles.
     */
    Observable<List<Article>> getArticles(@Nullable Object object);

}
