package homework.chegg.com.chegghomework.model;

/**
 * Created by reuvenlevitsky on 05/03/2018.
 */

/**
 * This class represents the Article model that we load and display.
 * Since I don't have enough time please forgive me for not documenting this class - it might the less interesting one in this project ;)
 */
public class Article {

    private String title;

    private String subtitle;

    private String imageUrl;

    // Default constructor is needed for serialization.
    public Article() {

    }

    public Article(String title, String subtitle, String imageUrl) {
        this.title = title;
        this.subtitle = subtitle;
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public String getSubtitle() {
        return this.subtitle;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
