package homework.chegg.com.chegghomework.utils.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by reuvenlevitsky on 22/07/2017.
 */

public class ViewWrapper<V extends View & Bindable> extends RecyclerView.ViewHolder {

    private V view;

    public ViewWrapper(V itemView) {
        super(itemView);
        view = itemView;
    }

    public V getView() {
        return view;
    }
}
