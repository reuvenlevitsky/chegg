package homework.chegg.com.chegghomework.utils.general;

import android.content.Context;

/**
 * Created by reuvenlevitsky on 03/02/2017.
 */

public class UnitsConversionUtils {

    /**
     * Returns the dp size of the given pixels.
     *
     * @param context
     * @param px pixels size.
     * @return
     */
    public static int convertPXtoDP(Context context, int px) {
        int dp = 0;
        if(px != 0) {
            dp = (int)(px / AppUtils.deviceDensity(context));
        }
        return dp;
    }

    /**
     * Returns the pixel size of the given dp.
     *
     * @param context
     * @param dp dp size.
     * @return
     */
    public static int convertDPtoPX(Context context, int dp) {
        int px = 0;
        if(dp != 0) {
            px = (int)(dp * AppUtils.deviceDensity(context));
        }
        return px;
    }

}
