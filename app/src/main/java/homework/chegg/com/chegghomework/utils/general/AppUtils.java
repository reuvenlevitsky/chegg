package homework.chegg.com.chegghomework.utils.general;

import android.content.Context;
import android.content.res.Configuration;

/**
 * Created by reuvenlevitsky on 11/2/15.
 */
public class AppUtils {

    public enum ScreenSizes {
        SMALL, NORMAL, HIGH, XLARGE
    }

    public static boolean isTablet(Context context){
        return isXLargeScreen(context) || isLargeScreen(context);
    }

    public static boolean isXLargeScreen(Context context) {
        return ScreenSizes.XLARGE.equals(getScreenSize(context));
    }

    public static boolean isLargeScreen(Context context) {
        return ScreenSizes.HIGH.equals(getScreenSize(context));
    }

    public static ScreenSizes getScreenSize(Context context) {
        ScreenSizes result = ScreenSizes.HIGH;
        // Determine screen size
        if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            result = ScreenSizes.HIGH;
        } else if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            result = ScreenSizes.NORMAL;
        } else if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            result = ScreenSizes.SMALL;
        } else if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4) {
            result = ScreenSizes.XLARGE;
        }

        return result;
    }

    public static float deviceDensity(Context context){
        float density = context.getResources().getDisplayMetrics().density;
        return density;
    }

    public static String playStoreStringURL(Context context){
        String appPackageName = context.getPackageName();
        String storeStringURL = "https://play.google.com/store/apps/details?id=" + appPackageName;
        return storeStringURL;
    }

}
