package homework.chegg.com.chegghomework.presenter;

/**
 * Created by reuvenlevitsky on 06/03/2018.
 */

/**
 * This is the interface that the Main Presenter in the app will implement.
 */
public interface MainPresenter {

    /**
     * Invoked when the view gets resumed.
     */
    void onResume();

    /**
     * Invoked when the view gets paused.
     */
    void onPause();

    /**
     * Invoked when the view asks for content reload.
     */
    void reloadData();

}
