package homework.chegg.com.chegghomework.datamodel;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import homework.chegg.com.chegghomework.cache.EndpointCache;
import homework.chegg.com.chegghomework.datamodel.parsers.EndpointParser;
import homework.chegg.com.chegghomework.model.Article;
import homework.chegg.com.chegghomework.network.EndpointNetworkLoader;
import io.reactivex.Observable;

/**
 * Created by reuvenlevitsky on 05/03/2018.
 */

/**
 * This class implements the dataModel of a single endpoint.
 */
public class EndpointDataModel implements EndpointDataModelInterface {

    /**
     * Module used for performing network request.
     */
    private EndpointNetworkLoader loader;

    /**
     * Module used for cache handling.
     */
    private EndpointCache cache;

    /**
     * Module used for parsing the network request response.
     */
    private EndpointParser parser;

    /**
     *
     * @param loader Module used for performing network request.
     * @param cache Module used for cache handling.
     * @param parser Module used for parsing the network request response.
     */
    public EndpointDataModel(EndpointNetworkLoader loader, EndpointCache cache, EndpointParser parser) {
        this.loader = loader;
        this.cache = cache;
        this.parser = parser;
    }

    @NonNull
    @Override
    public Observable<List<Article>> getArticles() {
        // Try to load from cache and if empty loads from network.
        // We are performing here a trick to stop if cache observer was emitting (By using `concat & first`).
        // If it was completed without emitting the network observer will start immediately.
        return Observable.concat(this.cache.loadFromCache(), this.getArticleFromNetwork())
                .first(new ArrayList<>())
                .toObservable();
    }

    /**
     *
     * @return The returned observable is responsible for the full process of loading article list from the network.
     */
    private Observable<List<Article>> getArticleFromNetwork() {
        return this.loader.loadFromNetwork() // Load from network.
                .flatMap(this.parser::getArticles) // Parse to list of articles.
                .doOnNext(articles -> {
                    // Save to cache the pared articles.
                    this.cache.cacheArticles(articles).subscribe();
                });
    }

}
