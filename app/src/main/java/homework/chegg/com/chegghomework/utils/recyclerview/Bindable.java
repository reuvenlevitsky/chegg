package homework.chegg.com.chegghomework.utils.recyclerview;

/**
 * Created by reuvenlevitsky on 22/07/2017.
 */

/**
 * This interface can be used for any classes that binding their view to some data. Like in cells.
 * @param <T>
 */
public interface Bindable<T> {

    void bind(T bindingObject);

}
