package homework.chegg.com.chegghomework.presenter;

import java.lang.ref.WeakReference;

import homework.chegg.com.chegghomework.datamodel.AppDataModel;
import homework.chegg.com.chegghomework.view.MainView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Created by reuvenlevitsky on 06/03/2018.
 */

/**
 * This class implements in our app the MainPresenter interface. It is the Presenter in the MVP architecture we use for this app.
 */
public class MainPresenterImpl implements MainPresenter {

    //------------------------------------- Vars -------------------------------------//

    /**
     * Reference to the view.
     */
    private WeakReference<MainView> view;

    /**
     * Reference to the model.
     */
    private AppDataModel appDataModel;

    /**
     * Holds the current loading task if there is any.
     */
    private Disposable currentLoadingTask;

    //------------------------------------- Constructors -------------------------------------//

    /**
     *
     * @param view The View in the MVP architecture.
     * @param appDataModel The model in the MVP architecture.
     */
    public MainPresenterImpl(MainView view, AppDataModel appDataModel) {
        this.view = new WeakReference(view);
        this.appDataModel = appDataModel;
    }

    //------------------------------------- MainView -------------------------------------//

    @Override
    public void onResume() {
        // When the view is resumed we load the data presented.
        this.reloadData();
    }

    @Override
    public void onPause() {
        // We want to clear here the current loading task in order to prevent it from being leaked.
        this.cleanCurrentLoadingTask();
    }

    @Override
    public void reloadData() {
        // First clear current task.
        this.cleanCurrentLoadingTask();

        // Show loading indicator.
        this.getView().showLoading();
        // Start the loading.
        this.currentLoadingTask = this.appDataModel.getArticles()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(articles -> {
                    getView().showData(articles);
                    getView().hideLoading();
                }, error -> {
                    getView().hideLoading();
                    getView().showError("There was an error loading the content, please try again later.");
                });
    }

    //------------------------------------- Private -------------------------------------//

    /**
     * Convenient method to return MainView.
     * @return the MainView of this instance.
     */
    private MainView getView() {
        return this.view.get();
    }

    /**
     * Stops and cleans the current loading task
     */
    private void cleanCurrentLoadingTask() {
        this.getView().hideLoading();

        if(this.currentLoadingTask != null) {
            if(!this.currentLoadingTask.isDisposed()) {
                this.currentLoadingTask.dispose();
            }

            this.currentLoadingTask = null;
        }
    }

}
