package homework.chegg.com.chegghomework.datamodel.parsers;

import android.support.annotation.Nullable;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import homework.chegg.com.chegghomework.model.Article;

/**
 * Created by reuvenlevitsky on 05/03/2018.
 */

public class Endpoint3Parser extends BaseEndpointParser {

    protected List<Article> parseArticles(@Nullable Object object) throws JSONException {
        List<Article> articles = new ArrayList<>();

        if (object != null && object instanceof ArrayList) {
            ArrayList<Map> articlesArray = (ArrayList) object;
            for (Map articleData : articlesArray) {
                String title = (String) articleData.get("topLine");

                String subtitle = ((String) articleData.get("subLine1")) + articleData.get("subline2");

                String imageUrl = (String) articleData.get("image");
                Article parsedArticle = new Article(title, subtitle, imageUrl);
                articles.add(parsedArticle);
            }
        }

        return articles;
    }
}
