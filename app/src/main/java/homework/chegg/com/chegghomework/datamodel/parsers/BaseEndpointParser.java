package homework.chegg.com.chegghomework.datamodel.parsers;

import android.support.annotation.Nullable;

import java.util.List;

import homework.chegg.com.chegghomework.model.Article;
import io.reactivex.Observable;

/**
 * Created by reuvenlevitsky on 05/03/2018.
 */

/**
 * Base class that saves some copy pasting of the `getArticles` method between the difference parsers
 * and basically enables them only implementing the actual parsing.
 */
public abstract class BaseEndpointParser implements EndpointParser {

    @Override
    public Observable<List<Article>> getArticles(final @Nullable Object object) {
        return Observable.just(object)
                .map(this::parseArticles);
    }

    /**
     * Subclasses should implement this method and parse the network response into array of articles.
     *
     * @param object - The result of the network request.
     * @return Parsed list of articles.
     * @throws Exception - Make sense to allow parsers to throw some exceptions if there is any error parsing.
     */
    protected abstract List<Article> parseArticles(@Nullable Object object) throws Exception;

}
