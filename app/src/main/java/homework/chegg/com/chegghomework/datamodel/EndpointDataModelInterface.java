package homework.chegg.com.chegghomework.datamodel;

import android.support.annotation.NonNull;

import java.util.List;

import homework.chegg.com.chegghomework.model.Article;
import io.reactivex.Observable;

public interface EndpointDataModelInterface {

    /**
     *
     * @return Observable for loading a list of articles from an endpoint.
     */
    @NonNull
    Observable<List<Article>> getArticles();

}
