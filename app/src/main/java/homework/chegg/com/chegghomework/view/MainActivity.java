package homework.chegg.com.chegghomework.view;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import homework.chegg.com.chegghomework.R;
import homework.chegg.com.chegghomework.adapters.MainViewRecyclerViewAdapter;
import homework.chegg.com.chegghomework.datamodel.AppDataModel;
import homework.chegg.com.chegghomework.model.Article;
import homework.chegg.com.chegghomework.presenter.MainPresenter;
import homework.chegg.com.chegghomework.presenter.MainPresenterImpl;
import homework.chegg.com.chegghomework.utils.general.UnitsConversionUtils;
import homework.chegg.com.chegghomework.utils.recyclerview.RecyclerViewMargin;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.menu_main_activity)
public class MainActivity extends AppCompatActivity implements MainView {

    @ViewById(R.id.toolbar)
    public Toolbar toolbar;

    @ViewById(R.id.my_recycler_view)
    public RecyclerView mRecyclerView;

    @ViewById(R.id.loading_indicator)
    public AVLoadingIndicatorView loadingIndicatorView;

    /**
     * A reference to the presenter in our MVP.
     */
    private MainPresenter presenter;

    /**
     * The adapter that is being used for the data handling of the recyclerView.
     * IMPORTANT - It doesn't mean this is equal to mRecyclerView.getAdapter because we use in this class animation
     * wrapper for the adapter.
     */
    private MainViewRecyclerViewAdapter recyclerViewAdapter;

    @AfterViews
    public void setup() {
        setSupportActionBar(toolbar);

        int margin = UnitsConversionUtils.convertDPtoPX(this, 10);
        RecyclerViewMargin decoration = new RecyclerViewMargin(margin, 1);
        this.mRecyclerView.addItemDecoration(decoration);

        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        this.recyclerViewAdapter = new MainViewRecyclerViewAdapter();
        ScaleInAnimationAdapter animatedAdapter = new ScaleInAnimationAdapter(this.recyclerViewAdapter);
        this.mRecyclerView.setAdapter(animatedAdapter);

        // Init the Presenter and the Model in our MVP.
        AppDataModel appDataModel = new AppDataModel(this);
        this.presenter = new MainPresenterImpl(this, appDataModel);
    }

    @Override
    protected void onResume() {
        super.onResume();

        this.presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        this.presenter.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        this.presenter = null;
    }

    @OptionsItem(R.id.action_refresh)
    void refreshSelected() {
        this.presenter.reloadData();
    }

    //------------------------------------- MainView -------------------------------------//

    @Override
    public void showLoading() {
        this.loadingIndicatorView.show();
        this.loadingIndicatorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        this.loadingIndicatorView.hide();
        this.loadingIndicatorView.setVisibility(View.GONE);
    }

    @Override
    public void showError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showData(List<Article> articles) {
        // We set the items on our adapter.
        this.recyclerViewAdapter.setItems(articles);
        // We notify the adapter directly from the recycler - we don't know if our MainAdapter is being wrapped by another
        // adapter or not.
        this.mRecyclerView.getAdapter().notifyDataSetChanged();
    }
}
