package homework.chegg.com.chegghomework.view;

import java.util.List;

import homework.chegg.com.chegghomework.model.Article;

/**
 * Created by reuvenlevitsky on 06/03/2018.
 */

/**
 * Interface that the View in our MVP is implementing.
 */
public interface MainView {

    /**
     * This method should show some loading indicator.
     */
    void showLoading();

    /**
     * This method should hide the loading indicator shown.
     */
    void hideLoading();

    /**
     * This method is responsible on showing an error message to the user.
     *
     * @param errorMessage The error message to present.
     */
    void showError(String errorMessage);

    /**
     * This method sets the data to the view. In our case the data is a list of articles that is being shown by our UI.
     *
     * @param articles The list of articles that should we shown to the user.
     */
    void showData(List<Article> articles);

}
