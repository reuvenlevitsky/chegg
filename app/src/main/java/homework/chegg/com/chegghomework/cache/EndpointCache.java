package homework.chegg.com.chegghomework.cache;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import homework.chegg.com.chegghomework.model.Article;
import io.reactivex.Observable;

/**
 * Created by reuvenlevitsky on 06/03/2018.
 */

/**
 * This module is used for caching the Articles loaded by Endpoints.
 */
public class EndpointCache {

    private int minutesToCache;
    private SharedPreferences sharedPreferences;

    private static final String LAST_CACHE_TIMESTAMP_KEY = "last_cache_timestamp";
    private static final String LAST_CACHE_ITEMS_KEY = "last_cache_items";

    /**
     *
     * @param cacheFileName Name of the file that will be used for this cache module.
     * @param minutesToCache if <= 0 cached will be always ignored.
     * @param context
     */
    public EndpointCache(@NonNull String cacheFileName, int minutesToCache, @NonNull Context context) {
        this.minutesToCache = minutesToCache;

        this.sharedPreferences = context.getSharedPreferences(cacheFileName, context.MODE_PRIVATE);
    }

    //------------------------------------- Public -------------------------------------//

    /**
     *
     * @return the articles from the cache or null if doesn't exist / out of date.
     */
    public Observable<List<Article>> loadFromCache() {
        return Observable.create(subscriber -> {
            List<Article> cachedArticles = this.loadFromCacheFile();
            // We emmit only if there are cached items. Otherwise we just report completion.
            if(cachedArticles != null) {
                subscriber.onNext(cachedArticles);
            }
            subscriber.onComplete();
        });
    }

    /**
     * Caches the given articles.
     *
     * @param articles The new article array to cache.
     * @return true if cache was written successfully.
     */
    public Observable<Boolean> cacheArticles(List<Article> articles) {
        return Observable.create(subscriber -> {
            boolean didSaveToCache = this.saveToCache(articles);
            subscriber.onNext(didSaveToCache);
            subscriber.onComplete();
        });
    }

    //------------------------------------- Private -------------------------------------//

    /**
     * Saves the articles to the sharedPref.
     *
     * @param articles The articles to save.
     * @return true if cache was written successfully.
     */
    private boolean saveToCache(List<Article> articles) {
        // Save the current timestamp.
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putLong(LAST_CACHE_TIMESTAMP_KEY, new Date().getTime());
        editor.putString(LAST_CACHE_ITEMS_KEY, new Gson().toJson(articles));
        return editor.commit();
    }

    /**
     * Loads the cached articles from the sharedPref if exists and not out of date.
     *
     * @return cached articles or null if there are no cached article / out of date.
     */
    private List<Article> loadFromCacheFile() {
        List<Article> cachedArticles = null;

        long lastCacheTimestamp = this.sharedPreferences.getLong(LAST_CACHE_TIMESTAMP_KEY, Long.MIN_VALUE);

        boolean shouldIgnoreCache = this.minutesToCache < 0 || // If minutesToCache is not defined we should always ignore cache.
                (lastCacheTimestamp > 0 && new Date().getTime() - lastCacheTimestamp > TimeUnit.MINUTES.toMillis(this.minutesToCache)); // If the current time is bigger than the cached time in more than the number of minutes defined we should ignore cache.

        if (!shouldIgnoreCache) {
            String cachedItemsString = this.sharedPreferences.getString(LAST_CACHE_ITEMS_KEY, null);
            if (cachedItemsString != null) {
                Gson gson = new Gson();
                cachedArticles = gson.fromJson(cachedItemsString, new TypeToken<List<Article>>(){}.getType());
            }
        }

        return cachedArticles;
    }

}
