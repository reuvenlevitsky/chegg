package homework.chegg.com.chegghomework.utils.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by reuvenlevitsky on 22/07/2017.
 */

public abstract class RecyclerViewAdapterBase<T, V extends View & Bindable> extends RecyclerView.Adapter<ViewWrapper<V>> {

    protected List<T> items;

    @Override
    public int getItemCount() {
        int itemsSize = 0;
        if(items != null) {
            itemsSize = items.size();
        }
        return itemsSize;
    }

    @Override
    public final ViewWrapper<V> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewWrapper<V>(onCreateItemView(parent, viewType));
    }

    protected abstract V onCreateItemView(ViewGroup parent, int viewType);

    public void onBindViewHolder(ViewWrapper viewHolder, int position) {
        View view = viewHolder.getView();
        T item = items.get(position);

        ((Bindable<T>) view).bind(item);
    }

    // additional methods to manipulate the items

    public void setItems(List<T> items) {
        this.items = items;
    }

    public List<T> getItems() {
        return items;
    }
}
