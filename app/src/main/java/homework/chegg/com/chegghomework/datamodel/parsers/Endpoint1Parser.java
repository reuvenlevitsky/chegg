package homework.chegg.com.chegghomework.datamodel.parsers;

import android.support.annotation.Nullable;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import homework.chegg.com.chegghomework.model.Article;

/**
 * Created by reuvenlevitsky on 05/03/2018.
 */

public class Endpoint1Parser extends BaseEndpointParser {

    protected List<Article> parseArticles(@Nullable Object object) throws JSONException {
        List<Article> articles = new ArrayList<>();

        if (object != null && object instanceof Map) {
            Map jsonMap = (Map) object;
            ArrayList<Map> articlesArray = (ArrayList) jsonMap.get("stories");
            for (Map articleData : articlesArray) {
                String title = (String) articleData.get("title");
                String subtitle = (String) articleData.get("subtitle");
                String imageUrl = (String) articleData.get("imageUrl");
                Article parsedArticle = new Article(title, subtitle, imageUrl);
                articles.add(parsedArticle);
            }
        }

        return articles;
    }
}
