package homework.chegg.com.chegghomework.datamodel.parsers;

import android.support.annotation.Nullable;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import homework.chegg.com.chegghomework.model.Article;

/**
 * Created by reuvenlevitsky on 05/03/2018.
 */

public class Endpoint2Parser extends BaseEndpointParser {

    protected List<Article> parseArticles(@Nullable Object object) throws JSONException {
        List<Article> articles = new ArrayList<>();

        if (object != null && object instanceof Map) {
            Map jsonMap = (Map) object;
            ArrayList<Map> articlesArray = (ArrayList<Map>) ((Map) jsonMap.get("metadata")).get("innerdata");
            for (Map articleData : articlesArray) {
                Map articleWrapper = (Map) articleData.get("articlewrapper");
                String title = (String) articleWrapper.get("header");
                String subtitle = (String) articleWrapper.get("description");

                String imageUrl = (String) articleData.get("picture");
                Article parsedArticle = new Article(title, subtitle, imageUrl);
                articles.add(parsedArticle);
            }
        }

        return articles;
    }
}
