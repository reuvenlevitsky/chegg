package homework.chegg.com.chegghomework.network;

import android.support.annotation.NonNull;

import homework.chegg.com.chegghomework.Consts;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by reuvenlevitsky on 06/03/2018.
 */

/**
 * This module is used for network requests to load the different endpoints.
 */
public class EndpointNetworkLoader {

    private String fileName;

    /**
     *
     * @param fileName The name of the json file that completes the base path into the endpoint's url.
     */
    public EndpointNetworkLoader(@NonNull String fileName) {
        this.fileName = fileName;
    }

    /**
     *
     * @return Observable that is responsible of loading the endpoints content into an object (Parsed by Gson).
     */
    public Observable<Object> loadFromNetwork() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Consts.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
        MyApiEndpointInterface apiService =
                retrofit.create(MyApiEndpointInterface.class);
        return apiService.getObject(this.fileName);
    }

    /**
     * Interface used by Retrofit to generate the service used for loading.
     */
    private interface MyApiEndpointInterface {
        @GET("/android/homework/{file_name}")
        Observable<Object> getObject(@Path("file_name") String fileName);
    }
}
