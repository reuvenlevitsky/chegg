package homework.chegg.com.chegghomework.adapters;

import android.content.Context;
import android.view.ViewGroup;

import homework.chegg.com.chegghomework.model.Article;
import homework.chegg.com.chegghomework.utils.recyclerview.RecyclerViewAdapterBase;
import homework.chegg.com.chegghomework.view.CardItemCellView;
import homework.chegg.com.chegghomework.view.CardItemCellView_;

/**
 * Created by reuvenlevitsky on 06/03/2018.
 */

/**
 * This class implements the adapter we use in the MainView's recyclerView.
 * It uses some base adapter I've written for my own app. It makes it much cleaner.
 */
public class MainViewRecyclerViewAdapter extends RecyclerViewAdapterBase<Article, CardItemCellView> {

    @Override
    protected CardItemCellView onCreateItemView(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        return CardItemCellView_.build(context);
    }

}
